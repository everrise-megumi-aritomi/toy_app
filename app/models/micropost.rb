class Micropost < ApplicationRecord
  # 一つのマイクロポストは一人のユーザーにのみ属する
  # micropostsテーブルにはuser_idカラムを作成してあるので、それによってユーザーと関連付けることができる
  belongs_to :user
  validates :content, length: { maximum: 140 }, presence: true
end
